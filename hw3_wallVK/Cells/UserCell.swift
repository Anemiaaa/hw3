//
//  UserCell.swift
//  hw3_wallVK
//
//  Created by mac on 11.07.2021.
//

import UIKit

class UserCell: UITableViewCell, CustomCell {

    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    
    static func nibCell() -> UINib? {
        return UINib(nibName: String(describing: self), bundle: nil)
    }
    
    static func idCell() -> String {
        return String(describing: self)
    }
    
    func config(with user: User) {
        cellImage.image = user.photo != nil ? user.photo : #imageLiteral(resourceName: "standartPic.jpeg")
        cellImage.layer.cornerRadius = cellImage.frame.size.width / 2
        cellImage.clipsToBounds = true
        cellLabel.text = user.name
    }
    
}
