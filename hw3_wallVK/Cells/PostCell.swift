//
//  PostCell.swift
//  hw3_wallVK
//
//  Created by mac on 15.07.2021.
//

import UIKit

class PostCell: UITableViewCell, CustomCell{
    
  //  @IBOutlet weak var imagePost: UIImageView!
   // @IBOutlet weak var userImage: UIImageView!
//    @IBOutlet weak var userName: UILabel!
//    @IBOutlet weak var userImagePost: UIImageView!
//    @IBOutlet weak var postLabelCell: UILabel!
    @IBOutlet weak var userImagePost: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var imagePost: UIImageView!
    @IBOutlet weak var postLabelCell: UILabel!
    
    static func nibCell() -> UINib? {
        return UINib(nibName: String(describing: self), bundle: nil)
    }
    
    static func idCell() -> String {
        return String(describing: self)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(with user: User, indexPath: IndexPath){
        if let currentPost = user.postsArray?[indexPath.row], currentPost.image != nil || currentPost.text != nil {
            userImagePost.image = user.photo
            userImagePost.layer.cornerRadius = userImagePost.frame.size.width / 2
            userImagePost.clipsToBounds = true

            userName.text = user.name
            
            if currentPost.image != nil {
                imagePost.isHidden = false
                imagePost.image = currentPost.image
            } else {
                imagePost.isHidden = true
            }
            
            if let f = currentPost.text {
                postLabelCell.isHidden = false
                postLabelCell.text = f
                postLabelCell.sizeToFit()
            } else {
                postLabelCell.isHidden = true
            }
        } 
    }
    
}
