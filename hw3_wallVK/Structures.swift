//
//  Structures.swift
//  hw3_wallVK
//
//  Created by mac on 11.07.2021.
//

import Foundation
import UIKit

struct User {
    let id = UUID().uuidString
    let name: String
    let login: String
    let password: String
    let photo: UIImage?
    let age: Int = 0
    let city: String
    let dateOfBirth: Date
    let work: String?
    let phoneNumb: String?
    let education: String?
    var postsArray: [UserPost]?
}

struct UserPost {
    let id = UUID().uuidString
    let image: UIImage?
    var text: String?
}

struct CurrentUser {
    private(set) var id: String?
    mutating func set(_ id: String) {
        if let _ = UUID(uuidString: id) {
            self.id = id
        }
    }
}

