//
//  UIViewController+Extensions.swift
//  hw3_wallVK
//
//  Created by Yaroslav Nosyk on 30.07.2021.
//

import UIKit

extension UIViewController {
    static func loadFromNib() -> Self {
        func instantiateFromNib<T: UIViewController>() -> T {
            return T.init(nibName: String(describing: T.self), bundle: nil)
        }

        return instantiateFromNib()
    }
}
