//
//  ProfileViewController.swift
//  hw3_wallVK
//
//  Created by mac on 13.07.2021.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageAndCityLabel: UILabel!
    @IBOutlet weak var dateOfBirthLabel: UILabel!
    @IBOutlet weak var workLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var educationLabel: UILabel!
    @IBOutlet weak var detailInfoView: UIView!
    
    //let idPosts = "idPosts"
    //let idUsers = "idUsers"
    
    
    var user: User?
    var onEdit: ((UserPost) -> ())?
    var onDelete: ((UserPost) -> ())?

    private var posts: [UserPost]?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    convenience init(user: User){
        self.init(nibName: String(describing: ProfileViewController.self), bundle: Bundle.main)
        self.user = user
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        detailInfoView.isHidden = true
        if let user = user {
        let calendar = Calendar.current
        let dontSet = "Не указано"
        imageProfile.image = user.photo
        nameLabel.text = user.name
        ageAndCityLabel.text = String(calendar.dateComponents([.year], from: user.dateOfBirth).year!) + ", " + (user.city )
        dateOfBirthLabel.text = dateToString(date: user.dateOfBirth)
        workLabel.text =  user.work != nil ? user.work : dontSet
        phoneNumberLabel.text = user.phoneNumb != nil ? user.phoneNumb : dontSet
        educationLabel.text = user.work != nil ? user.education : dontSet
        
        imageProfile.layer.cornerRadius = imageProfile.frame.size.width / 2
        imageProfile.clipsToBounds = true
        }
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == idPosts, let Model = sender as? User {
//            
//            let destController = segue.destination as! PostsViewController
//            destController.user = Model
//            
//            destController.onEdit = { [weak self] post in
//                guard let self = self,
//                      let index = self.user?.postsArray?.firstIndex(where: { $0.id == post.id }) else { return }
//                
//                self.user?.postsArray?.remove(at: index)
//                self.user?.postsArray?.insert(post, at: index)
//                
//                self.onEdit?(post)
//            }
//            
//            destController.onDelete = { [weak self] post in
//                guard let self = self,
//                      let index = self.user?.postsArray?.firstIndex(where: { $0.id == post.id }) else { return }
//                self.onDelete?(post)
//                
//                self.user?.postsArray?.remove(at: index)
//            }
//        }
//        
//        //if segue.identifier == idUsers, 
//    }

    @IBAction func isPressedPostsSegueButton(_ sender: Any) {
        //performSegue(withIdentifier: idPosts, sender: user)
        if let user = user {
            let postsController = PostsViewController(user: user)
            
            postsController.onEdit = { [weak self] post in
                guard let self = self,
                      let index = self.user?.postsArray?.firstIndex(where: { $0.id == post.id }) else { return }
                
                self.user?.postsArray?.remove(at: index)
                self.user?.postsArray?.insert(post, at: index)
                
                self.onEdit?(post)
            }
            
            postsController.onDelete = { [weak self] post in
                guard let self = self,
                      let index = self.user?.postsArray?.firstIndex(where: { $0.id == post.id }) else { return }
                self.onDelete?(post)
                
                self.user?.postsArray?.remove(at: index)
            }
            
            let backButton = UIBarButtonItem()
            backButton.title = "Профиль"
            
            navigationItem.backBarButtonItem = backButton
            navigationController?.pushViewController(postsController, animated: true)
        }
    }

    @IBAction func isPressedUsersButton(_ sender: Any) {
        
    }
    
    @IBAction func isPressedDetailButton(_ sender: Any) {
        detailInfoView.isHidden.toggle()
        
        self.view.layoutIfNeeded()
    }
    
    private func dateToString(date: Date?) -> String{
        let dateFormater = DateFormatter()
        if let notNilDate = date {
            return dateFormater.string(from: notNilDate)
        } else {
            return ""
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
