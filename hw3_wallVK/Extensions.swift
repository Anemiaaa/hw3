//
//  TableViewExtension.swift
//  hw3_wallVK
//
//  Created by mac on 12.07.2021.
//

import Foundation
import UIKit

protocol CustomCell {
    
    static func nibCell() -> UINib?
    
    static func idCell() -> String
}

extension UITableView {
    func registerCustomCell(_ cell: CustomCell.Type) {
        self.register(cell.nibCell(), forCellReuseIdentifier: cell.idCell())
    }
}

extension UIViewController {
    
    func customizingNavigationBar() {
        self.navigationController?.navigationBar.barTintColor  = UIColor(red: 89 / 255, green: 125 / 255, blue: 163 / 255, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
}

extension String {
    static func randomWord(minSize: Int, maxSize: Int, _ firstCapitalLetter: Bool) -> String {
        let vowels: [Character] = ["a", "e", "i", "o", "u", "y"]
        let consonants: [Character] = ["b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "y", "z"]
        let size = Int.random(in: minSize ... maxSize)
        var randomString = ""
        
        for index in 1 ... size{
            randomString += index % 2 == 0 ? String(vowels[Int.random(in: 0 ..< vowels.count)]) : String(consonants[Int.random(in: 0 ..< consonants.count)])
        }
        return firstCapitalLetter == true ? randomString.capitalized : randomString
    }
    static func randomWord(minSize: Int, maxSize: Int) -> String {
       return String.randomWord(minSize: minSize, maxSize: maxSize, true)
    }
}
