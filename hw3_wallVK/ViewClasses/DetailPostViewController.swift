//
//  DetailPostViewController.swift
//  hw3_wallVK
//
//  Created by mac on 16.07.2021.
//

import UIKit

class DetailPostViewController: UIViewController {
    
    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var nameUser: UILabel!
    @IBOutlet weak var imagePost: UIImageView!
    @IBOutlet weak var postLabel: UILabel!
    
    var name: String?
    var photo: UIImage?
    var post: UserPost?
    
    var onDelete: ((UserPost) -> ())?
    var onEdit: ((UserPost) -> ())?
    
 //   let idEditText = "idEditText"
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    convenience init(userName: String, userPhoto: UIImage?, post: UserPost?){
        self.init(nibName: String(describing: DetailPostViewController.self), bundle: Bundle.main)
        self.name = userName
        self.photo = userPhoto
        self.post = post
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setFields()
    }
    
    @IBAction func didPressedActionButton(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let editButton = UIAlertAction(title: "Редактировать", style: .default) { (action) in
            if let _ = self.post {
                //self.performSegue(withIdentifier: self.idEditText, sender: self.post)
                let editController = EditPostViewController(nibName: "EditPostViewController", bundle: nil)
                
                editController.post = self.post
                editController.onEdit = { [weak self] text in
                    guard let self = self else { return }
                    
                    self.post?.text = text
                    self.setPostText()
                    if let post = self.post {
                        self.onEdit?(post)
                    }
                }
                //self.navigationController?.pushViewController(editController, animated: true)
                self.present(editController, animated: true, completion: nil)
            } else {
                let errorAlert = UIAlertController(title: "Ошибка", message: "Нет текста для редактирования", preferredStyle: .alert)
                
                let cancelErrorButton = UIAlertAction(title: "ОК", style: .cancel, handler: nil)
                
                errorAlert.addAction(cancelErrorButton)
                
                self.present(errorAlert, animated: true, completion: nil)
            }
        }
        
        let delButton = UIAlertAction(title: "Удалить", style: .destructive) { (action) in
            let delAlert = UIAlertController(title: nil, message: "Вы действительно хотите удалить пост?", preferredStyle: .actionSheet)
            
            let delConfirmButton = UIAlertAction(title: "Да", style: .default) { (action) in
                if let post = self.post {
                    self.onDelete?(post)
                    self.navigationController?.popViewController(animated: true)
                }
            }
            let delCancelButton = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
            
            delAlert.addAction(delConfirmButton)
            delAlert.addAction(delCancelButton)
            
            self.present(delAlert, animated: true, completion: nil)
        }
        
        let cancelButton = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        alert.addAction(editButton)
        alert.addAction(delButton)
        alert.addAction(cancelButton)
        
        self.present(alert, animated: true, completion: nil)
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == idEditText, let post = sender as? UserPost {
//
//            let destController = segue.destination as! EditPostViewController
//
//            destController.post = post
//            destController.onEdit = { [weak self] text in
//                guard let self = self else { return }
//
//                self.post?.text = text
//                self.setPostText()
//                if let post = self.post {
//                    self.onEdit?(post)
//                }
//            }
//        }
//    }
    
    private func setFields() {
        imageUser.image = photo
        imageUser.layer.cornerRadius = imageUser.frame.size.width / 2
        imageUser.clipsToBounds = true
        
        nameUser.text = name
        
        setPostText()
        
        if let img = post?.image {
            imagePost.image = img
        } else {
            imagePost.isHidden = true
        }
    }
    
    private func setPostText() {
        if let text = post?.text {
            postLabel.text = text
            postLabel.sizeToFit()
            view.setNeedsLayout()
        } else {
            postLabel.isHidden = true
        }
    }

}
