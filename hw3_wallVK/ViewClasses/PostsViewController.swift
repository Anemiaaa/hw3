//
//  PostsViewController.swift
//  hw3_wallVK
//
//  Created by mac on 15.07.2021.
//

import UIKit

class PostsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
  //  let idDetailPost = "idDetailPost"
    
    var user: User?
    var onEdit: ((UserPost) -> ())?
    var onDelete: ((UserPost) -> ())?
    
    private var selectedIndex = 0
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    convenience init(user: User){
        self.init(nibName: String(describing: PostsViewController.self), bundle: Bundle.main)
        self.user = user
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.registerCustomCell(PostCell.self)
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 400
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return user?.postsArray?.count ?? 0
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == idDetailPost {
//            
//            let destController = segue.destination as! DetailPostViewController
//            
//            destController.name = user?.name
//            destController.photo = user?.photo
//            destController.post = user?.postsArray?[selectedIndex]
//            destController.onDelete = { [weak self] post in
//                guard let self = self,
//                      let index = self.user?.postsArray?.firstIndex(where: { $0.id == post.id }) else { return }
//                
//                self.user?.postsArray?.remove(at: index)
//                
//                self.onDelete?(post)
//                
//                self.tableView.reloadData()
//            }
//            destController.onEdit = { [weak self] post in
//                guard let self = self,
//                      let index = self.user?.postsArray?.firstIndex(where: { $0.id == post.id }) else { return }
//                
//                self.user?.postsArray?.remove(at: index)
//                self.user?.postsArray?.insert(post, at: index)
//                
//                self.onEdit?(post)
//                
//                self.tableView.reloadData()
//            }
//        }
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let user = user {
            selectedIndex = indexPath.row
            
            let detailPostController = DetailPostViewController(userName: user.name, userPhoto: user.photo, post: user.postsArray?[selectedIndex])
            
            detailPostController.onDelete = { [weak self] post in
                guard let self = self,
                      let index = self.user?.postsArray?.firstIndex(where: { $0.id == post.id }) else { return }
                
                self.user?.postsArray?.remove(at: index)
                
                self.onDelete?(post)
                
                self.tableView.reloadData()
            }
            detailPostController.onEdit = { [weak self] post in
                guard let self = self,
                      let index = self.user?.postsArray?.firstIndex(where: { $0.id == post.id }) else { return }
                
                self.user?.postsArray?.remove(at: index)
                self.user?.postsArray?.insert(post, at: index)
                
                self.onEdit?(post)
                
                self.tableView.reloadData()
            }
            
            let backButton = UIBarButtonItem()
            backButton.title = " "
            
            navigationItem.backBarButtonItem = backButton
            navigationController?.pushViewController(detailPostController, animated: true)
        }
    
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PostCell.idCell(), for: indexPath) as! PostCell
        
        if let model = user {
            cell.config(with: model, indexPath: indexPath)
        }
        
        return cell
    }
    
    func deleteUserPost(post: UserPost) {
        
    }
}
