//
//  ViewControllerUsers.swift
//  hw3_wallVK
//
//  Created by mac on 11.07.2021.
//

import UIKit

class ViewControllerUsers: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var users: [User]?
    @IBOutlet weak var tableView: UITableView!
    
    let idToProfile = "idProfile"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self

        tableView.registerCustomCell(UserCell.self)
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 400
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == idToProfile, let Model = sender as? User, let _ = self.users {
            
            let destController = segue.destination as! ProfileViewController
            
            destController.user = Model
            
            destController.onEdit = { [weak self] post in
                guard let self = self,
                      let userToEdit = self.users?.enumerated().first(where: {$0.element.id == Model.id}),
                      let index = userToEdit.element.postsArray?.firstIndex(where: { $0.id == post.id }) else { return }
                
                self.users?[userToEdit.offset].postsArray?.remove(at: index)
                self.users?[userToEdit.offset].postsArray?.insert(post, at: index)
            }
            
            destController.onDelete = { [weak self] post in
                guard let self = self,
                      let userToDelete = self.users?.enumerated().first(where: {$0.element.id == Model.id}),
                      let index = userToDelete.element.postsArray?.firstIndex(where: { $0.id == post.id }) else { return }
                
                self.users?[userToDelete.offset].postsArray?.remove(at: index)
            }

        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let model = users?[indexPath.row]{
            performSegue(withIdentifier: idToProfile, sender: model)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath:IndexPath) -> CGFloat{
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: UserCell.idCell(), for: indexPath) as! UserCell
        if let model = users?[indexPath.row]{
            cell.config(with: model)
        }

        return cell
    }

}


