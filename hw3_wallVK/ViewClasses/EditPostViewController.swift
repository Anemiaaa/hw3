//
//  EditPostViewController.swift
//  hw3_wallVK
//
//  Created by mac on 21.07.2021.
//

import UIKit

class EditPostViewController: UIViewController {
    
    @IBOutlet weak var textView: UITextView!
    
    var post: UserPost?
    var onEdit: ((String) -> ())?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        textView.text = post?.text
    }
    

    @IBAction func didPressedSaveButton(_ sender: Any) {
                onEdit?(textView.text ?? "")
        
        dismiss(animated: true, completion: nil)
       // self.navigationController?.popViewController(animated: true)
    }
    
}
