//
//  LoginViewController.swift
//  hw3_wallVK
//
//  Created by mac on 27.07.2021.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var inputText: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    
    private var users: [User] = []
    //var errors: [String: Array<String>]
    var errors = ["login": [], "password": []] as [String : Array<String>]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customizingNavigationBar()
        
        fillUsers()
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        passwordTextField.isSecureTextEntry = true
        emailTextField.placeholder = "Логин"
        passwordTextField.placeholder = "Пароль"
        
        submitButton.backgroundColor = UIColor(red: 89 / 255, green: 125 / 255, blue: 163 / 255, alpha: 1.0)
        submitButton.setTitleColor(UIColor.white, for: .normal)
        
//        addBottomLineToTextField(textField: emailTextField, colorOfLine: UIColor(red: 89 / 255, green: 125 / 255, blue: 163 / 255, alpha: 1.0))
//        addBottomLineToTextField(textField: passwordTextField, colorOfLine: UIColor(red: 89 / 255, green: 125 / 255, blue: 163 / 255, alpha: 1.0))
    }
    
    @IBAction func isPressedSubmitButton(_ sender: Any) {
        if let login = emailTextField.text, let password = passwordTextField.text {
            if !isValid(email: login){
                if let loginAlert = createAlert(titleAlert: "Ошибка поля ввода логина!", messageALert: errors["login"]?.first, titleButton: "OK") {
                    present(loginAlert, animated: true, completion: { return })
                }
            }
            
            if !isValid(password: password) {
                if let passwordAlert = createAlert(titleAlert: "Ошибка поля ввода пароля!", messageALert: errors["password"]?.first, titleButton: "OK") {
                    present(passwordAlert, animated: true, completion: nil)
                }
                return
            }
            
            if let user = users.first(where: { $0.login == login }) {
                if user.password == password {
                    let profileController = ProfileViewController(user: user)
                    
                    profileController.onEdit = { [weak self] post in
                        guard let self = self,
                              let userToEdit = self.users.enumerated().first(where: {$0.element.id == user.id}),
                              let index = userToEdit.element.postsArray?.firstIndex(where: { $0.id == post.id }) else { return }
                        
                        self.users[userToEdit.offset].postsArray?.remove(at: index)
                        self.users[userToEdit.offset].postsArray?.insert(post, at: index)
                    }
                    
                    profileController.onDelete = { [weak self] post in
                        guard let self = self,
                              let userToDelete = self.users.enumerated().first(where: {$0.element.id == user.id}),
                              let index = userToDelete.element.postsArray?.firstIndex(where: { $0.id == post.id }) else { return }
                        
                        self.users[userToDelete.offset].postsArray?.remove(at: index)
                    }
                    
                    navigationController?.pushViewController(profileController, animated: true)
                } else {
                    if let wrongPasswordAlert = createAlert(titleAlert: "Ошибка", messageALert: "Неверный пароль!", titleButton: "OK"){
                        present(wrongPasswordAlert, animated: true, completion: nil)
                    }
                }
            } else {
                if let userNotFoundAlert = createAlert(titleAlert: "Ошибка", messageALert: "Пользователя с таким логином не найдено!", titleButton: "OK") {
                    present(userNotFoundAlert, animated: true, completion: nil)
                }
            }
        
            
        }
//        else {
//            if let emptyFieldsAlert = createAlert(titleAlert: "Ошибка", messageALert: "Заполните все поля!", titleButton: "OK"){
//                present(emptyFieldsAlert, animated: true, completion: nil)
//            }
//        }
    }
    
    private func isValid(password: String) -> Bool {
        let passwordPred = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-z])(?=.*[0-9]).{6,}$")
        let isValid = passwordPred.evaluate(with: password)
        
        isValid == true ? errors["password"]?.removeAll() : errors["password"]?.append("Неверный формат пароля!")
        return isValid
    }
    
    private func isValid(email: String) -> Bool {
        let emailPred = NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}")
        let isValid = emailPred.evaluate(with: email)
        
        isValid == true ? errors["login"]?.removeAll() : errors["login"]?.append("Неверный формат логина!")
        return isValid
    }
    
    private func createAlert(titleAlert: String?, messageALert: String?, titleButton: String?) -> UIAlertController? {
        if let _ = titleAlert, let _ = messageALert, let _ = titleButton {
            let alert = UIAlertController(title: titleAlert, message: messageALert, preferredStyle: .alert)
            let cancelButton = UIAlertAction(title: titleButton, style: .cancel, handler: nil)
            
            alert.addAction(cancelButton)
            return alert
        } else {
            return nil
        }
    }

//    private func addBottomLineToTextField(textField: UITextField, colorOfLine: UIColor){
//        let bottomLine = CALayer()
//
//        bottomLine.frame = CGRect(x: 0.0, y: textField.frame.height - 1, width: textField.frame.width, height: 1.0)
//        bottomLine.backgroundColor = colorOfLine.cgColor
//
//        textField.borderStyle = UITextField.BorderStyle.none
//        textField.layer.addSublayer(bottomLine)
//    }
    
    private func fillUsers() {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        var posts: [UserPost] = []
        
        posts.append(UserPost(image: #imageLiteral(resourceName: "littlePrince.jpeg"), text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur tincidunt odio suscipit sapien dignissim porta. Aliquam sed tortor et enim placerat dignissim. Ut bibendum egestas venenatis. Donec a auctor tortor."))
        
        posts.append(UserPost(image: nil, text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra augue id magna lacinia tristique. Maecenas dictum gravida placerat. Sed lorem neque, bibendum ac ex id, auctor fermentum est. Cras sollicitudin metus a odio scelerisque ultricies. Maecenas ante nibh, pharetra sit amet augue id, interdum varius urna. Sed posuere lectus.//nLorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra augue id magna lacinia tristique. Maecenas dictum gravida placerat. Sed lorem neque, bibendum ac ex id, auctor fermentum est. Cras sollicitudin metus a odio scelerisque ultricies. Maecenas ante nibh, pharetra sit amet augue id, interdum varius urna. Sed posuere lectus.\\n//nLorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra augue id magna lacinia tristique. Maecenas dictum gravida placerat. Sed lorem neque, bibendum ac ex id, auctor fermentum est. Cras sollicitudin metus a odio scelerisque ultricies. Maecenas ante nibh, pharetra sit amet augue id, interdum varius urna. Sed posuere lectus.\\n//nLorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra augue id magna lacinia tristique. Maecenas dictum gravida placerat. Sed lorem neque, bibendum ac ex id, auctor fermentum est. Cras sollicitudin metus a odio scelerisque ultricies. Maecenas ante nibh, pharetra sit amet augue id, interdum varius urna. Sed posuere lectus.\\n//nLorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra augue id magna lacinia tristique. Maecenas dictum gravida placerat. Sed lorem neque, bibendum ac ex id, auctor fermentum est. Cras sollicitudin metus a odio scelerisque ultricies. Maecenas ante nibh, pharetra sit amet augue id, interdum varius urna. Sed posuere lectus.\\n"))
        posts.append(UserPost(image: #imageLiteral(resourceName: "standartPic.jpeg"), text: nil))
        
        users.append(User(name: "Rose",
                          login: "rose@gmail.com",
                          password: "rose123",
                          photo: #imageLiteral(resourceName: "littlePrince"),
                          city: String.randomWord(minSize: 2, maxSize: 7),
                          dateOfBirth: formatter.date(from: "2000/10/08")!,
                          work: nil,
                          phoneNumb: "38097" + String(Int.random(in: 1000000 ... 9999999)),
                          education: String.randomWord(minSize: 4, maxSize: 9) + String.randomWord(minSize: 5, maxSize: 8, false),
                          postsArray: posts))
        
        users.append(User(name: "David",
                          login: "david@gmail.com",
                          password: "david123",
                          photo: #imageLiteral(resourceName: "photo"),
                          city: String.randomWord(minSize: 2, maxSize: 7),
                          dateOfBirth: formatter.date(from: "1993/12/21")!,
                          work: String.randomWord(minSize: 6, maxSize: 9),
                          phoneNumb: "38097" + String(Int.random(in: 1000000 ... 9999999)),
                          education: String.randomWord(minSize: 4, maxSize: 9) + String.randomWord(minSize: 5, maxSize: 8, false),
                          postsArray: nil))
        
        users.append(User(name: "Sandra",
                          login: "sandra@gmail.com",
                          password: "sandra123",
                          photo: #imageLiteral(resourceName: "girl"),
                          city: String.randomWord(minSize: 2, maxSize: 7),
                          dateOfBirth: formatter.date(from: "1996/03/17")!,
                          work: String.randomWord(minSize: 6, maxSize: 9),
                          phoneNumb: "38097" + String(Int.random(in: 1000000 ... 9999999)),
                          education: String.randomWord(minSize: 4, maxSize: 9) + String.randomWord(minSize: 5, maxSize: 8, false),
                          postsArray: nil))
        
        users.append(User(name: "Nancy",
                          login: "nancy@gmail.com",
                          password: "nancy123",
                          photo: nil,
                          city: String.randomWord(minSize: 2, maxSize: 7),
                          dateOfBirth: formatter.date(from: "2005/07/30")!,
                          work: nil,
                          phoneNumb: "38097" + String(Int.random(in: 1000000 ... 9999999)),
                          education: nil,
                          postsArray: nil))
        }
    

}


extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print()
        return true
    }
}
