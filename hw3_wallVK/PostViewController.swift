//
//  ViewController.swift
//  hw3_wallVK
//
//  Created by mac on 11.07.2021.
//

import UIKit

class PostViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableViewPost: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableViewPost.delegate = self
        tableViewPost.dataSource = self
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableViewPost.dequeueReusableCell(withIdentifier: "postCell", for: indexPath)
            
        return cell
    }

}

